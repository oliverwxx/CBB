/*
 * Copyright (c) 2023 Huawei Technologies Co.,Ltd.
 *
 * CBB is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *          http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * mes_recv.h
 *
 *
 * IDENTIFICATION
 *    src/cm_mes/mes_recv.h
 *
 * -------------------------------------------------------------------------
 */
#ifndef __MES_RECV_H__
#define __MES_RECV_H__
#include "cm_types.h"
#include "mes_interface.h"
#include "cm_epoll.h"
#include "mes_func.h"

int mes_start_receivers(uint32 priority_count, unsigned int *recv_task_count, mes_event_proc_t event_proc);
void mes_stop_receivers();
int mes_add_recv_pipe_to_epoll(uint16 channel_id, mes_priority_t priority, uint32 version, int sock);
int mes_remove_recv_pipe_from_epoll(mes_priority_t priority, uint32 channel_id, int sock);
int mes_start_sender_monitor();
void mes_stop_sender_monitor();
int mes_add_send_pipe_to_epoll(uint16 channel_id, mes_priority_t priority, uint32 version, int sock);
int mes_remove_send_pipe_from_epoll(mes_priority_t priority, uint32 channel_id, int sock);
void mes_get_receiver_thread(mes_thread_set_t *mes_thread_set);

#endif